package com.example.fooddeliveryapp;

import androidx.annotation.NonNull;

import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;

import java.util.ArrayList;
import java.util.HashMap;

public class Restaurant {
    // Properties
    public String name;
    public String description;
    public int imageResource;
    public int stars;
    public int deliveryFee;
    public ArrayList<Menu> menus = new ArrayList<>();
    public HashMap<String,Food> allItemsHashMap = new HashMap<>();

    Restaurant(int imageResource) {
        this.imageResource = imageResource;
    }

    // Populates the properties with random values
    public void populateRandomProperties() {
        int maxStars = 5;
        int minStars = 1;
        int maxDeliveryFee = 999;
        int minDeliveryFee = 0;
        Lorem lorem = LoremIpsum.getInstance();

        this.stars = (int)(Math.random() * (maxStars - minStars + 1) + minStars);
        this.deliveryFee = (int)(Math.random() * (maxDeliveryFee - minDeliveryFee + 1) + minDeliveryFee);
        this.name = lorem.getWords(1, 4);
        this.description = lorem.getParagraphs(1, 1);
    }
//    }
}
