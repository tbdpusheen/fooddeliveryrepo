package com.example.fooddeliveryapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

public class CategoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        connectXMLViews();
        setupListener();

        System.out.println("CategoryActivity created");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Category category = DataModel.getInstance(this).retrieveCategories().get(getIntent().getExtras().getInt("com.example.fooddeliveryapp.CATEGORY"));
        categoryImage.setImageResource(category.imageResource);
        categoryName.setText(category.name);
        restaurantArrayList = category.restaurants;

        setupRecyclerView();

        System.out.println("CategoryActivity populated with " + category.name);
    }

    // Sets up the restaurant recyclerView
    private void setupRecyclerView() {
        categoryRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        categoryRecyclerView.setAdapter(new RestaurantAdapter(this, restaurantArrayList, this));
    }

    // Connect the layout's XML Views
    private void connectXMLViews() {
        categoryImage = findViewById(R.id.category_image);
        categoryName = findViewById(R.id.category_name);
        categoryRecyclerView = findViewById(R.id.category_recycler_view);
        bottomNavigationView = findViewById(R.id.bottomNavigationView);
    }

    // Sets up listeners for user interaction
    private void setupListener() {
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            if (item.getItemId() == R.id.home_page) {
                Intent intent = new Intent(this, HomeActivity.class);
                this.startActivity(intent);
                return true;
            }
            else if (item.getItemId() == R.id.checkout_page) {
                Intent intent = new Intent(this, CheckoutActivity.class);
                this.startActivity(intent);
                return true;
            }
            return false;
        });
    }

    // ViewHolder for the restaurant recyclerView
    private static class RestaurantViewHolder extends RecyclerView.ViewHolder {
        public RestaurantViewHolder(@NonNull View itemView) {
            super(itemView);
            restaurantImage = itemView.findViewById(R.id.restaurant_image);
            restaurantNameTextView = itemView.findViewById(R.id.restaurant_name);
            restaurantDeliveryFeeTextView = itemView.findViewById(R.id.delivery_fee);
        }

        //Properties
        private final ImageView restaurantImage;
        private final TextView restaurantNameTextView;
        private final TextView restaurantDeliveryFeeTextView;
    }

    // Adapter for the restaurant recyclerView
    private static class RestaurantAdapter extends RecyclerView.Adapter<RestaurantViewHolder> {

        // Constructor
        RestaurantAdapter(@NonNull Context context, @NonNull ArrayList<Restaurant> restaurants, @NonNull CategoryActivity categoryActivity) {
            this.context = context;
            this.restaurants = restaurants;
            this.categoryActivity = categoryActivity;

        }
        // Overridden methods
        @NonNull
        @Override
        public RestaurantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            // Method called whenever new ViewHolder needs to be created
            LayoutInflater inflater = LayoutInflater.from(context);
            View itemView = inflater.inflate(R.layout.item_restaurant, parent, false);
            return new RestaurantViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull RestaurantViewHolder holder, int position) {
            // Method called whenever existing ViewHolder needs to be reused
            // Repopulate ViewHolder
            Restaurant restaurant = restaurants.get(position);
            holder.restaurantImage.setImageResource(restaurant.imageResource);
            holder.restaurantNameTextView.setText(restaurant.name);
            String deliveryFee;
            if (restaurant.deliveryFee == 0) {
                deliveryFee = "Free";
            }
            else if (restaurant.deliveryFee/100 == 0) {
                deliveryFee = "$0." + restaurant.deliveryFee % 100;
            }
            else {
                deliveryFee = "$" + restaurant.deliveryFee/100 + "." + restaurant.deliveryFee % 100;
            }
            holder.restaurantDeliveryFeeTextView.setText(deliveryFee);

            // Creates an onClickLister which activates when the holder is clicked
            holder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(categoryActivity, RestaurantActivity.class);
                intent.putExtra(EXTRA_RESTAURANT, DataModel.getInstance(categoryActivity).retrieveRestaurants().indexOf(restaurant));
                System.out.println("(Restaurant) " + restaurant.name + " selected");
                categoryActivity.startActivity(intent);
            });
        }

        @Override
        public int getItemCount() {
            return restaurants.size();
        }

        // Properties
        private final Context context;
        private final ArrayList<Restaurant> restaurants;
        private final CategoryActivity categoryActivity;
    }

    // Properties
    public static final String EXTRA_RESTAURANT = "com.example.fooddeliveryapp.RESTAURANT";
    public ArrayList<Restaurant> restaurantArrayList = new ArrayList<>();
    // XML Views
    private ImageView categoryImage;
    private TextView categoryName;
    private RecyclerView categoryRecyclerView;
    private BottomNavigationView bottomNavigationView;
}