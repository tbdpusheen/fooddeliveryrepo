package com.example.fooddeliveryapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Objects;

public class AddressActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);

        // Gets the user that logged from the login page
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            user = extras.getString(Intent.EXTRA_USER);
        } else {
            user = (String)savedInstanceState.getSerializable(Intent.EXTRA_USER);
        }

        connectXMLViews();
        setupArraylist();
        setupRecyclerView();
        setupListeners();
        
        System.out.println("AddressActivity created");
    }

    // Public methods
    // Opens the create new address menu by setting the visibility of the views to visible
    public void openNewAddressMenu(View v) {
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) newAddressBackground.getLayoutParams();
        params.height = 450;
        newAddressBackground.setLayoutParams(params);

        newAddressInstructionsEditText.setVisibility(View.VISIBLE);
        saveNewAddressButton.setVisibility(View.VISIBLE);
        saveNewAddressBackground.setVisibility(View.VISIBLE);
        newAddressMenuButton.setVisibility(View.INVISIBLE);
        System.out.println("New address menu expanded");
    }

    // Private methods
    // Connects the XML views from the layout
    private void connectXMLViews() {
        previousActivityButton = findViewById(R.id.previous_activity_button);
        newAddressEditText = findViewById(R.id.new_address_editText);
        newAddressInstructionsEditText = findViewById(R.id.new_address_instructions_editText);
        newAddressBackground = findViewById(R.id.new_address_background);
        newAddressMenuButton = findViewById(R.id.new_address_menu_button);
        saveNewAddressBackground = findViewById(R.id.save_address_button_background);
        saveNewAddressButton = findViewById(R.id.save_new_address_button);
        addressRecyclerView = findViewById(R.id.address_recyclerView);
        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        System.out.println("Address XML views connected");
    }

    // Sets up the addresses arrayList using the address file for the current user
    @SuppressWarnings("unchecked")
    private void setupArraylist() {
        try {
            if (new File(this.getFilesDir(), user + " addresses").exists()) {
                FileInputStream fis = this.openFileInput(user + " addresses");
                ObjectInputStream ois = new ObjectInputStream(fis);
                addresses = (ArrayList<Address>) ois.readObject();
                ois.close();
                fis.close();
                System.out.println("Addresses arrayList filled from file");
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    // Sets up listeners for user interaction
    private void setupListeners() {
        // Creates an onClickListener for the save address button
        saveNewAddressButton.setOnClickListener(v -> {
            // Saves the new address to the addresses arrayList and refreshes the recyclerView
            Address newAddress = new Address();
            newAddress.name = newAddressEditText.getText().toString();
            newAddress.instructions = newAddressInstructionsEditText.getText().toString();
            addresses.add(0, newAddress);
            Objects.requireNonNull(addressRecyclerView.getAdapter()).notifyItemInserted(0);

            // If the address file associated with the current user already exists, delete it to refresh the addresses
            if (new File(this.getFilesDir(), user + " addresses").delete()) {
                System.out.println("Previous address file for " + user + " deleted");
            }
            // Opens a new file associated with the current user that stores the addresses arrayList
            try {
                FileOutputStream fos = this.openFileOutput(user + " addresses", MODE_PRIVATE);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(addresses);
                oos.close();
                fos.close();
                System.out.println("Added current addresses to addresses file");
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Closes the new address menu
            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) newAddressBackground.getLayoutParams();
            params.height = 130;
            newAddressBackground.setLayoutParams(params);
            newAddressInstructionsEditText.setVisibility(View.INVISIBLE);
            saveNewAddressButton.setVisibility(View.INVISIBLE);
            saveNewAddressBackground.setVisibility(View.INVISIBLE);
            newAddressMenuButton.setVisibility(View.VISIBLE);
            newAddressEditText.setText("");
            newAddressInstructionsEditText.setText("");

            System.out.println("(Address) " + newAddress.name + " added with instructions:\n" + newAddress.instructions);
        });

        // Create the onClickListener for the previous activity button
        previousActivityButton.setOnClickListener(v -> finish());

        // Creates the listener for when an item is selected from the bottomNavigationView
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            if (item.getItemId() == R.id.search_page) {
                Intent intent = new Intent(getParent(), SearchActivity.class);
                getParent().startActivity(intent);
                return true;
            }
            else if (item.getItemId() == R.id.checkout_page) {
                Intent intent = new Intent(getParent(), CheckoutActivity.class);
                getParent().startActivity(intent);
                return true;
            }
            return false;
        });
    }

    // Sets up the addresses recyclerView
    private void setupRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        AddressAdapter adapter = new AddressAdapter(this, addresses, this);
        addressRecyclerView.setLayoutManager(layoutManager);
        addressRecyclerView.setAdapter(adapter);
        System.out.println("Address recyclerView setup complete");
    }

    // Classes
    // ViewHolder for the addresses recyclerView
    private static class AddressViewHolder extends RecyclerView.ViewHolder {
        public AddressViewHolder(@NonNull View itemView) {
            super(itemView);
            addressText = itemView.findViewById(R.id.address_text);
            addressInstructionsText = itemView.findViewById(R.id.address_instructions_text);
            deleteButton = itemView.findViewById(R.id.delete_icon);
        }

        // Properties
        private final TextView addressText;
        private final TextView addressInstructionsText;
        private final ImageButton deleteButton;
    }

    // Adapter for the addresses recyclerView
    private static class AddressAdapter extends RecyclerView.Adapter<AddressViewHolder> {

        AddressAdapter(@NonNull Context context, @NonNull ArrayList<Address> addresses, @NonNull AddressActivity addressActivity) {
            this.context = context;
            this.addresses = addresses;
            this.addressActivity = addressActivity;
        }

        @NonNull
        @Override
        public AddressViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View itemView = inflater.inflate(R.layout.item_address, parent, false);
            return new AddressViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull AddressViewHolder holder, int position) {
            Address address = addresses.get(position);
            holder.addressText.setText(address.name);
            holder.addressInstructionsText.setText(address.instructions);

            // Sets an onClickListener for the itemView
            holder.itemView.setOnClickListener(v -> {
                // Sets the current address to the address selected and exits the activity
                String currentAddress = "Delivering to: " + address.name;
                Intent intent = new Intent(addressActivity, HomeActivity.class);
                intent.putExtra(EXTRA_ADDRESS, currentAddress);
                intent.putExtra(EXTRA_INSTRUCTIONS, address.instructions);
                System.out.println("(Address) " + holder.addressText.getText() + " selected");
                addressActivity.startActivity(intent);
            });

            // Sets an onClickListener for the delete button
            holder.deleteButton.setOnClickListener(v -> {
                addresses.remove(position);
                Objects.requireNonNull(addressActivity.addressRecyclerView.getAdapter()).notifyItemRemoved(position);
                System.out.println("(Address) " + holder.addressText.getText() + " deleted");
            });
        }

        @Override
        public int getItemCount() { return addresses.size(); }

        // Properties
        private final Context context;
        private final ArrayList<Address> addresses;
        private final AddressActivity addressActivity;
    }

    // Properties
    public static final String EXTRA_ADDRESS = "com.example.fooddeliveryapp.ADDRESS";
    public static final String EXTRA_INSTRUCTIONS = "com.example.fooddeliveryapp.INSTRUCTIONS";
    private ArrayList<Address> addresses = new ArrayList<>();
    private String user;
    // XML Views
    private EditText newAddressEditText;
    private EditText newAddressInstructionsEditText;
    private View newAddressBackground;
    private View saveNewAddressBackground;
    private View newAddressMenuButton;
    private Button saveNewAddressButton;
    private RecyclerView addressRecyclerView;
    private BottomNavigationView bottomNavigationView;
    private FloatingActionButton previousActivityButton;
}