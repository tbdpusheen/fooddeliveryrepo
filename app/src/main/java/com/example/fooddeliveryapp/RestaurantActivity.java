package com.example.fooddeliveryapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

public class RestaurantActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);

        connectXMLViews();
        setupListeners();

        System.out.println("RestaurantActivity created");
    }

    @Override
    protected void onStart() {
        super.onStart();
        restaurant = DataModel.getInstance(this).retrieveRestaurants().get(getIntent().getExtras().getInt("com.example.fooddeliveryapp.RESTAURANT"));
        restaurantImageView.setImageResource(restaurant.imageResource);
        restaurantTextView.setText(restaurant.name);
        for (Menu menu:restaurant.menus) {
            switch (menu.name) {
                case "Main":
                    mainMenuArrayList = menu.foodItems;
                    break;
                case "Sides":
                    sideDishesArrayList = menu.foodItems;
                case "Drinks":
                    drinksArrayList = menu.foodItems;
                    break;
                case "Desserts":
                    dessertsArrayList = menu.foodItems;
                    break;
            }
        }

        setupRecyclerViews();

        System.out.println("RestaurantActivity populated with " + restaurant.name);
    }

    // Setup recyclerViews
    private void setupRecyclerViews() {
        mainMenuRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mainMenuRecyclerView.setAdapter(new MenuAdapter(this, mainMenuArrayList, this));
        sideDishesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        sideDishesRecyclerView.setAdapter(new MenuAdapter(this, sideDishesArrayList, this));
        drinksRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        drinksRecyclerView.setAdapter(new MenuAdapter(this, drinksArrayList, this));
        dessertsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        dessertsRecyclerView.setAdapter(new MenuAdapter(this, dessertsArrayList, this));
    }

    // Sets up listeners for user interaction
    private void setupListeners() {
        //Main menu button
        mainMenuButton.setOnClickListener(v -> {
            menusScrollView.scrollTo(mainMenuRecyclerView.getScrollX(), mainMenuRecyclerView.getScrollY());
            System.out.println("Scrolled to main menu");
        });

        //Side dishes button
        sideDishesButton.setOnClickListener(v -> {
            menusScrollView.scrollTo(mainMenuRecyclerView.getScrollX(), mainMenuRecyclerView.getScrollY());
            System.out.println("Scrolled to sides");
        });

        //Drinks button
        drinksButton.setOnClickListener(v -> {
            System.out.println("Scrolled to drinks");
            menusScrollView.scrollTo(mainMenuRecyclerView.getScrollX(), mainMenuRecyclerView.getScrollY());
        });

        //Desserts button
        dessertsButton.setOnClickListener(v -> {
            System.out.println("Scrolled to desserts");
            menusScrollView.scrollTo(dessertsRecyclerView.getScrollX(), dessertsRecyclerView.getScrollY());
        });

        // BottomNavigationView buttons
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            if (item.getItemId() == R.id.home_page) {
                Intent intent = new Intent(this, HomeActivity.class);
                this.startActivity(intent);
                return true;
            }
            else if (item.getItemId() == R.id.search_page) {
                Intent intent = new Intent(this, SearchActivity.class);
                this.startActivity(intent);
                return true;
            }
            return false;
        });
    }

    // Connects the layout's XML views
    private void connectXMLViews() {
        mainMenuButton = findViewById(R.id.main_menu_button);
        sideDishesButton = findViewById(R.id.side_dishes_button);
        drinksButton = findViewById(R.id.drinks_button);
        dessertsButton = findViewById(R.id.desserts_button);
        mainMenuRecyclerView = findViewById(R.id.main_course_recyclerView);
        sideDishesRecyclerView = findViewById(R.id.side_dishes_recyclerView);
        drinksRecyclerView = findViewById(R.id.drinks_recyclerView);
        dessertsRecyclerView = findViewById(R.id.deserts_recyclerView);
        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        menusScrollView = findViewById(R.id.menus_scrollView);
        restaurantImageView = findViewById(R.id.restaurant_image);
        restaurantTextView = findViewById(R.id.restaurant_name);
    }

    // ViewHolder for the menu recyclerView
    public static class MenuViewHolder extends RecyclerView.ViewHolder {
        public MenuViewHolder (@NonNull View itemView) {
            super(itemView);
            itemImage = itemView.findViewById(R.id.item_image);
            itemNameTextView = itemView.findViewById(R.id.item_name);
            itemDescriptionTextView = itemView.findViewById(R.id.item_description);
            itemPriceTextView = itemView.findViewById(R.id.item_price);
        }

        //Properties
        private final ImageView itemImage;
        private final TextView itemNameTextView;
        private final TextView itemDescriptionTextView;
        private final TextView itemPriceTextView;
    }

    // Adapter for the menu recyclerView
    public static class MenuAdapter extends RecyclerView.Adapter<MenuViewHolder> {

        // Constructor
        MenuAdapter (@NonNull Context context, @NonNull ArrayList<Food> items, @NonNull RestaurantActivity restaurantActivity) {
            this.context = context;
            this.items = items;
            this.restaurantActivity = restaurantActivity;

        }
        // Overridden methods
        @NonNull
        @Override
        public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            // Method called whenever new ViewHolder needs to be created
            LayoutInflater inflater = LayoutInflater.from(context);
            View itemView = inflater.inflate(R.layout.item_food, parent, false);
            return new MenuViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull MenuViewHolder holder, int position) {
            // Method called whenever existing ViewHolder needs to be reused
            Food item = items.get(position);
            holder.itemImage.setImageResource(item.imageResource);
            holder.itemNameTextView.setText(item.name);
            holder.itemDescriptionTextView.setText(item.description);
            String price;
            if (item.price == 0) {
                price = "Free";
            }
            else if (item.price/100 == 0) {
                price = "$0." + item.price % 100;
            }
            else {
                price = "$" + item.price/100 + "." + item.price % 100;
            }
            holder.itemPriceTextView.setText(price);

            // Creates an onClickLister which activates when the holder is clicked
            holder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(restaurantActivity, FoodItemActivity.class);
                intent.putExtra(EXTRA_RESTAURANT, DataModel.getInstance(restaurantActivity).retrieveRestaurants().indexOf(restaurantActivity.restaurant));
                intent.putExtra(EXTRA_ITEM, item.name);
                System.out.println(item.name + " was selected");
                restaurantActivity.startActivity(intent);
            });
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        // Properties
        private final Context context;
        private final ArrayList<Food> items;
        private final RestaurantActivity restaurantActivity;
    }

    //Properties
    public static final String EXTRA_RESTAURANT = "com.example.fooddeliveryapp.RESTAURANT";
    public static final String EXTRA_ITEM = "com.example.fooddeliveryapp.ITEM";
    private Restaurant restaurant;
    private ArrayList<Food> mainMenuArrayList = new ArrayList<>();
    private ArrayList<Food> sideDishesArrayList = new ArrayList<>();
    private ArrayList<Food> drinksArrayList = new ArrayList<>();
    private ArrayList<Food> dessertsArrayList = new ArrayList<>();

    //XML Views
    private Button mainMenuButton;
    private Button sideDishesButton;
    private Button drinksButton;
    private Button dessertsButton;
    private ImageView restaurantImageView;
    private TextView restaurantTextView;
    private RecyclerView mainMenuRecyclerView;
    private RecyclerView sideDishesRecyclerView;
    private RecyclerView dessertsRecyclerView;
    private RecyclerView drinksRecyclerView;
    private BottomNavigationView bottomNavigationView;
    private NestedScrollView menusScrollView;
}