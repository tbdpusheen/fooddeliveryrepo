package com.example.fooddeliveryapp;

import android.app.Activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class DataModel {
    // Public methods
    // Singleton pattern
    public static DataModel getInstance(Activity activity) {
        if (sharedInstance == null) {
            sharedInstance = new DataModel();
            sharedInstance.populateDataModel(activity);
        }
        return sharedInstance;
    }

    // Retrieves the wanted properties
    public ArrayList<Category> retrieveCategories() { return categoryArrayList; }
    public ArrayList<Restaurant> retrieveRestaurants() { return restaurantsArrayList; }
    public HashMap<String,Restaurant> retrieveRestaurantsHashMap() { return restaurantHashmap; }

    // Private methods
    // Private Constructor for Singleton
    private DataModel() {}

    // Populates the data model using the data_model raw resource
    private void populateDataModel(Activity activity) {
        int imageResource;
        String menuName;

        // Creates a new inputStream from the data_model raw resource and tries to read from it
        InputStream inputStream = activity.getResources().openRawResource(R.raw.data_model);
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line = reader.readLine();

            // While there are still categories, continue adding categories to the category list
            while (line != null) {
                Category category = new Category("Error", R.drawable.default_category_image);

                // Sets the new category's properties to the read data
                category.name = line.replaceFirst("C ", "");
                line = reader.readLine();
                imageResource = activity.getResources().getIdentifier(line, "drawable", activity.getApplicationInfo().packageName);
                if (imageResource != 0) {
                    category.imageResource = imageResource;
                }
                else {
                    category.imageResource = R.drawable.default_category_image;
                }
                line = reader.readLine();

                // Continues adding new restaurants to the current category until a new category is read
                while (line != null && !line.startsWith("C ")) {
                    Restaurant restaurant = new Restaurant(R.drawable.default_restaurant_image);
                    restaurant.name = "Restaurant Not Found";
                    restaurant.description = "Description Not Found";
                    restaurant.stars = 0;
                    restaurant.deliveryFee = 0;

                    // Sets the new restaurant's properties to the read data
                    restaurant.name = line.replaceFirst("R ", "");
                    line = reader.readLine();
                    imageResource = activity.getResources().getIdentifier(line, "drawable", activity.getApplicationInfo().packageName);
                    if (imageResource != 0) {
                        restaurant.imageResource = imageResource;
                    }
                    else {
                        restaurant.imageResource = R.drawable.default_restaurant_image;
                    }
                    line = reader.readLine();
                    restaurant.description = line;
                    line = reader.readLine();
                    int convertedInteger = 0;
                    try {
                        convertedInteger = Integer.parseInt(line);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    restaurant.stars = convertedInteger;
                    line = reader.readLine();
                    try {
                        convertedInteger = Integer.parseInt(line);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    restaurant.deliveryFee = convertedInteger;
                    line = reader.readLine();

                    // Continues adding menus to the current restaurant until a new restaurant is read
                    while (line != null && !line.startsWith("R ") && !line.startsWith("C ")) {
                        // Specifies the menu which the foods arrayList will add into
                        ArrayList<Food> foods = new ArrayList<>();
                        menuName = line.replaceFirst("M ", "");
                        line = reader.readLine();

                        // Continues adding food items to a food arrayList until a new menu is read
                        while (line != null && !line.startsWith("M ") && !line.startsWith("R ") && !line.startsWith("C ")) {
                            Food food = new Food(R.drawable.default_item_image);
                            food.name = "Item not found";
                            food.description = "Description not found";
                            food.price = 0;
                            food.mostPopular = false;

                            // Sets the new food item's properties to the read data
                            food.name = line;
                            line = reader.readLine();
                            imageResource = activity.getResources().getIdentifier(line, "drawable", activity.getApplicationInfo().packageName);
                            if (imageResource != 0) {
                                food.imageResource = imageResource;
                            }
                            else {
                                food.imageResource = R.drawable.default_item_image;
                            }
                            line = reader.readLine();
                            food.description = line;
                            line = reader.readLine();
                            try {
                                convertedInteger = Integer.parseInt(line);
                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                            food.price = convertedInteger;
                            line = reader.readLine();
                            food.mostPopular = Boolean.getBoolean(line);
                            line = reader.readLine();

                            foods.add(food);
                            System.out.println("(Food Item) " + food.name + " added");
                        }

                        Menu menu = new Menu(menuName, foods);
                        restaurant.menus.add(menu);
                        for (Food food:foods) {
                            restaurant.allItemsHashMap.put(food.name, food);
                        }
                        System.out.println("Added current item list to " + menu);
                    }

                    category.restaurants.add(restaurant);
                    restaurantHashmap.put(restaurant.name, restaurant);
                    restaurantsArrayList.add(restaurant);
                    System.out.println("(Restaurant) " + restaurant.name + " added");
                }

                categoryArrayList.add(category);
                System.out.println("(Category) " + category.name + " added");
            }

            System.out.println(restaurantHashmap);
            System.out.println("Finished populating data model from data_model");
        } catch (IOException e) {
            e.printStackTrace();
        }

        randomlyGenerateDataModel();
    }

    // Populates the data model with random dishes and restaurants
    private void randomlyGenerateDataModel() {
        // Goes through every category inside of the category arrayList added in the data_model file
        for (Category category:categoryArrayList) {
            ArrayList<Restaurant> restaurants = new ArrayList<>();

            // If the current category is empty, fill it random restaurants that contain random foods
            if (category.restaurants.isEmpty()) {
                int restaurantsMax = 14;
                int restaurantsMin = 7;

                for (int i = 0; i < (int)(Math.random() * (restaurantsMax - restaurantsMin + 1) + restaurantsMin); i++) {
                    Restaurant restaurant = new Restaurant(R.drawable.default_restaurant_image);
                    restaurant.populateRandomProperties();
                    ArrayList<Food> foods = new ArrayList<>();

                    addToMenu(restaurant, "Main", foods);
                    addToMenu(restaurant, "Sides", foods);
                    addToMenu(restaurant, "Drinks", foods);
                    addToMenu(restaurant, "Desserts", foods);

                    restaurants.add(restaurant);
                    restaurantHashmap.put(restaurant.name, restaurant);
                    restaurantsArrayList.add(restaurant);
                    System.out.println("(Restaurant) " + restaurant.name + " added to " + category.name + " category");
                }
            }

            category.restaurants.addAll(restaurants);
        }

        System.out.println("Total Categories: " + categoryArrayList.size());
        System.out.println("Total Restaurants: " + restaurantHashmap.size());
    }

    // Adds the specified foods to the specified menu of the specified restaurant
    private void addToMenu(Restaurant restaurant, String menu, ArrayList<Food> foods) {
        // Properties
        int mainMenuMax = 8;
        int mainMenuMin = 4;
        int sidesMax = 6;
        int sidesMin = 3;
        int drinksMax = 4;
        int drinksMin = 2;
        int dessertsMax = 4;
        int dessertsMin = 1;

        int menuMax = 1;
        int menuMin = 1;

        // Adjusts the maximum and minimum item amount based on the current menu
        switch (menu) {
            case "Main":
                menuMax = mainMenuMax;
                menuMin = mainMenuMin;
                break;
            case "Sides":
                menuMax = sidesMax;
                menuMin = sidesMin;
                break;
            case "Drinks":
                menuMax = drinksMax;
                menuMin = drinksMin;
                break;
            case "Desserts":
                menuMax = dessertsMax;
                menuMin = dessertsMin;
                break;
        }

        // Adds a random amount of dishes within the set range to each menu
        for (int i = 0; i < (int)(Math.random() * (menuMax - menuMin + 1) + menuMax); i++) {
            Food food = new Food(R.drawable.default_item_image);
            food.PopulateRandomProperties();
            foods.add(food);
            System.out.println("(Food) " + food.name + " added to " + menu);
        }
        restaurant.menus.add(new Menu(menu, foods));
        System.out.println(menu + " added to " + restaurant.name);
        for (Food food:foods) {
            restaurant.allItemsHashMap.put(food.name, food);
        }
    }

    // Properties
    private static DataModel sharedInstance = null;

    public HashMap<String,Restaurant> restaurantHashmap = new HashMap<>();
    public ArrayList<Restaurant> restaurantsArrayList = new ArrayList<>();
    public ArrayList<Category> categoryArrayList = new ArrayList<>();
}
