package com.example.fooddeliveryapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // Checks to see if this activity is being recreated
        if (savedInstanceState != null) { user = (String) savedInstanceState.getSerializable(Intent.EXTRA_USER); }

        connectXMLViews();
        populateDataModel();
        // Checks if this is the first time creating this activity
        if (savedInstanceState == null) { populateArrayLists(); }
        setupRecyclerViews();
        scheduleFeaturedDish();
        setupListeners();

        System.out.println("HomeActivity created");
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Gets the extras from the intent which started this activity
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey("com.example.fooddeliveryapp.ADDRESS")) {
                currentAddressText.setText(extras.getString("com.example.fooddeliveryapp.ADDRESS"));
                System.out.println("Current address is " + extras.getString("com.example.fooddeliveryapp.ADDRESS"));
            }
            if (extras.containsKey(Intent.EXTRA_USER)) {
                user = extras.getString(Intent.EXTRA_USER);
                System.out.println("Current user is " + user);
            }
        }

        System.out.println("HomeActivity Started");
    }

    // Opens the food menu for the current featured image
    public void selectedCurrentFeaturedImage(View v) {
        Intent intent = new Intent(this, FoodItemActivity.class);
        intent.putExtra(EXTRA_RESTAURANT, featuredDishesRestaurant.get(featuredDishPosition).name);
        intent.putExtra(EXTRA_ITEM, featuredDishes.get(featuredDishPosition).name);
        System.out.println("New FoodItemActivity started for featured dish " + featuredDishPosition);
        startActivity(intent);
    }

    // Private methods
    // Connects this activity to the XML views
    private void connectXMLViews() {
        featuredImage = findViewById(R.id.featured_image);
        featuredImageButton1 = findViewById(R.id.featured_image_button_1);
        featuredImageButton2 = findViewById(R.id.featured_image_button_2);
        featuredImageButton3 = findViewById(R.id.featured_image_button_3);
        featuredImageButton4 = findViewById(R.id.featured_image_button_4);
        featuredImageButton5 = findViewById(R.id.featured_image_button_5);
        selectAddressButton = findViewById(R.id.select_address_button);
        currentAddressText = findViewById(R.id.current_address_text);
        miniCategoryRecyclerView = findViewById(R.id.mini_category_recyclerView);
        recommendedRecyclerView = findViewById(R.id.recommended_recyclerView);
        discountsRecyclerView = findViewById(R.id.discounts_recyclerView);
        popularRecyclerView = findViewById(R.id.popular_recyclerView);
        highestRatedRecyclerView = findViewById(R.id.highest_rated_recyclerView);
        bottomNavigationView = findViewById(R.id.bottomNavigationView);

        System.out.println("Connected XML Views");
    }

    // Gets an instance of the dataModel and retrieves its properties
    private void populateDataModel() {
        // Also populates the dataModel
        DataModel dataModel = DataModel.getInstance(this);

        categoryArrayList = dataModel.retrieveCategories();
        restaurantsArrayList = dataModel.retrieveRestaurants();
        restaurantHashmap = dataModel.retrieveRestaurantsHashMap();
    }

    // Populates the arrayLists that feed into each recyclerView with the restaurants specified
    private void populateArrayLists() {
        // Populates with some hand-picked restaurants
        recommendedArrayList.add(restaurantHashmap.get("Good Hunter"));
        popularArrayList.add(restaurantHashmap.get("Good Hunter"));
        popularArrayList.add(restaurantHashmap.get("Wanmin Restaurant"));
        highestRatedArrayList.add(restaurantHashmap.get("Wanmin Restaurant"));

        // Populates the rest with random restaurants
        randomlyPopulateArrayList(recommendedArrayList);
        randomlyPopulateArrayList(popularArrayList);
        randomlyPopulateArrayList(discountsArrayList);
        randomlyPopulateArrayList(highestRatedArrayList);

        System.out.println("ArrayLists populated");

        // Also populates the featured dishes arrayList with the desired dishes' images
        addToFeaturedDishes("Wanmin Restaurant", "Adeptus' Temptation");
        addToFeaturedDishes("Good Hunter", "Sticky Honey Roast");
        addToFeaturedDishes("Good Hunter", "Mondstadt Hash Brown");
        addToFeaturedDishes("Wanmin Restaurant", "Jade Parcels");
        addToFeaturedDishes("Wanmin Restaurant", "Zhongyuan Chop Suey");

        System.out.println("Featured images ArrayList populated");
    }

    // Sets up the various recyclerViews
    private void setupRecyclerViews() {
        miniCategoryRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        miniCategoryRecyclerView.setAdapter(new CategoryAdapter(this, categoryArrayList, this));

        recommendedRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recommendedRecyclerView.setAdapter(new RestaurantAdapter(this, recommendedArrayList, this));

        discountsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        discountsRecyclerView.setAdapter(new RestaurantAdapter(this, discountsArrayList, this));

        popularRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        popularRecyclerView.setAdapter(new RestaurantAdapter(this, popularArrayList, this));

        highestRatedRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        highestRatedRecyclerView.setAdapter(new RestaurantAdapter(this, highestRatedArrayList, this));

        System.out.println("Finished setting up recyclerViews");
    }

    // Creates a new schedule at a fixed rate that will continuously update the featured image
    private void scheduleFeaturedDish() {
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate(() -> runOnUiThread(() -> {
            setFeaturedDish(featuredDishPosition);
            featuredDishPosition++;
        }), 0, 8, TimeUnit.SECONDS);
    }

    // Sets up listeners for user interactions
    private void setupListeners() {
        featuredImageButton1.setOnClickListener(v -> setFeaturedDish(0));
        featuredImageButton2.setOnClickListener(v -> setFeaturedDish(1));
        featuredImageButton3.setOnClickListener(v -> setFeaturedDish(2));
        featuredImageButton4.setOnClickListener(v -> setFeaturedDish(3));
        featuredImageButton5.setOnClickListener(v -> setFeaturedDish(4));

        selectAddressButton.setOnClickListener(v -> {
            Intent intent = new Intent(this, AddressActivity.class);
            intent.putExtra(Intent.EXTRA_USER, user);
            startActivity(intent);
            System.out.println("Switched to delivery details screen");
        });

        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            if (item.getItemId() == R.id.search_page) {
                Intent intent = new Intent(this, SearchActivity.class);
                this.startActivity(intent);
                return true;
            }
            else if (item.getItemId() == R.id.checkout_page) {
                Intent intent = new Intent(this, CheckoutActivity.class);
                this.startActivity(intent);
                return true;
            }
            return false;
        });
    }

    // Sets the featured dish to the specified position and sets the featuredImageButtons accordingly
    private void setFeaturedDish(int position) {
        featuredDishPosition = position;
        if (featuredDishPosition == featuredDishes.size()) {
            featuredDishPosition = 0;
            System.out.println("Reset featured dish position");
        }
        featuredImage.setImageDrawable(ContextCompat.getDrawable(this, featuredDishes.get(featuredDishPosition).imageResource));

        featuredImageButton1.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.unselected_dot_image));
        featuredImageButton2.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.unselected_dot_image));
        featuredImageButton3.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.unselected_dot_image));
        featuredImageButton4.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.unselected_dot_image));
        featuredImageButton5.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.unselected_dot_image));

        switch (position) {
            case 0:
                featuredImageButton1.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.selected_dot_image));
                break;
            case 1:
                featuredImageButton2.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.selected_dot_image));
                break;
            case 2:
                featuredImageButton3.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.selected_dot_image));
                break;
            case 3:
                featuredImageButton4.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.selected_dot_image));
                break;
            case 4:
                featuredImageButton5.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.selected_dot_image));
                break;
        }

        System.out.println("Featured image changed to position " + featuredDishPosition);
    }

    // Populates the arrayList specified with random restaurants
    private void randomlyPopulateArrayList(ArrayList<Restaurant> arrayList) {
        int arrayListSizeMax = 7;
        int arrayListSizeMin = 4;

        for (int i = 0; i < (int)(Math.random() * (arrayListSizeMax - arrayListSizeMin + 1) + arrayListSizeMin); i++) {
            Restaurant randomRestaurant = restaurantsArrayList.get((int)(Math.random() * (restaurantsArrayList.size())));
            arrayList.add(randomRestaurant);
            System.out.println(randomRestaurant.name + " added to a homepage arrayList");
        }
    }

    // Adds the specified food in the specified restaurant to the featured dishes arrayList
    private void addToFeaturedDishes(String restaurant, String foodItem) {
        featuredDishes.add(Objects.requireNonNull(restaurantHashmap.get(restaurant)).allItemsHashMap.get(foodItem));
        featuredDishesRestaurant.add(restaurantHashmap.get(restaurant));
        System.out.println(Objects.requireNonNull((Objects.requireNonNull(restaurantHashmap.get(restaurant))).allItemsHashMap.get(foodItem)).name + " added to featured dishes");
    }

    // Private classes
    // ViewHolder for the restaurants inside of the different recyclerViews
    private static class RestaurantViewHolder extends RecyclerView.ViewHolder {
        private RestaurantViewHolder(@NonNull View itemView) {
            super(itemView);
            activityHomeRestaurantImage = itemView.findViewById(R.id.activity_home_restaurant_image);
            activityHomeRestaurantName = itemView.findViewById(R.id.activity_home_restaurant_name);
            activityHomeRestaurantDelivery = itemView.findViewById(R.id.activity_home_restaurant_delivery);
        }

        private final ImageView activityHomeRestaurantImage;
        private final TextView activityHomeRestaurantName;
        private final TextView activityHomeRestaurantDelivery;
    }

    // Adapter for the restaurants inside of the different recyclerViews
    private static class RestaurantAdapter extends RecyclerView.Adapter<RestaurantViewHolder> {

        // Constructor
        private RestaurantAdapter(@NonNull Context context, @NonNull ArrayList<Restaurant> restaurants, @NonNull HomeActivity homeActivity) {
            this.context = context;
            this.restaurants = restaurants;
            this.homeActivity = homeActivity;
        }

        @NonNull
        @Override
        public RestaurantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            // Inflates the viewHolder with the layout specified
            LayoutInflater inflater = LayoutInflater.from(context);
            View itemView = inflater.inflate(R.layout.item_activity_home_restaurant, parent, false);
            return new RestaurantViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull RestaurantViewHolder holder, int position) {
            // Sets the values of the viewHolder to that of the current position in the list
            Restaurant restaurant = restaurants.get(position);
            holder.activityHomeRestaurantImage.setImageResource(restaurant.imageResource);
            holder.activityHomeRestaurantName.setText(restaurant.name);
            String deliveryFee;
            if (restaurant.deliveryFee == 0) {
                deliveryFee = "Free";
            }
            else if (restaurant.deliveryFee/100 == 0) {
                deliveryFee = "$0." + restaurant.deliveryFee % 100;
            }
            else {
                deliveryFee = "$" + restaurant.deliveryFee/100 + "." + restaurant.deliveryFee % 100;
            }
            holder.activityHomeRestaurantDelivery.setText(deliveryFee);

            // Creates an onClickLister which activates when the holder is clicked
            holder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(homeActivity, RestaurantActivity.class);
                intent.putExtra(EXTRA_RESTAURANT, DataModel.getInstance(homeActivity).retrieveRestaurants().indexOf(restaurant));
                System.out.println("(Restaurant) " + restaurant.name + " selected");
                homeActivity.startActivity(intent);
            });
        }

        @Override
        public int getItemCount() {
            return restaurants.size();
        }

        // Properties
        private final Context context;
        private final ArrayList<Restaurant> restaurants;
        private final HomeActivity homeActivity;
    }

    // ViewHolder for the categories inside of the mini category recyclerView
    private static class CategoryViewHolder extends RecyclerView.ViewHolder {
        private CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            miniCategoryImageView = itemView.findViewById(R.id.mini_category_image);
            miniCategoryTextView = itemView.findViewById(R.id.mini_category_text);
        }

        private final ImageView miniCategoryImageView;
        private final TextView miniCategoryTextView;
    }

    // Adapter for the categories inside of the mini category recyclerView
    private static class CategoryAdapter extends RecyclerView.Adapter<CategoryViewHolder> {

        // Constructor
        private CategoryAdapter(@NonNull Context context, @NonNull ArrayList<Category> categories, @NonNull HomeActivity homeActivity) {
            this.context = context;
            this.categories = categories;
            this.homeActivity = homeActivity;
        }

        @NonNull
        @Override
        public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            // Inflates the viewHolder with the layout specified
            LayoutInflater inflater = LayoutInflater.from(context);
            View itemView = inflater.inflate(R.layout.item_mini_category, parent, false);
            return new CategoryViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
            // Sets the values of the viewHolder to that of the current position in the list
            Category category = categories.get(position);
            holder.miniCategoryImageView.setImageResource(category.imageResource);
            holder.miniCategoryTextView.setText(category.name);

            // Creates an onClickLister which activates when the holder is clicked
            holder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(homeActivity, CategoryActivity.class);
                intent.putExtra(EXTRA_CATEGORY, position);
                System.out.println("(Category) " + category.name + " selected");
                homeActivity.startActivity(intent);
            });
        }

        @Override
        public int getItemCount() {
            return categories.size();
        }

        // Properties
        private final Context context;
        private final ArrayList<Category> categories;
        private final HomeActivity homeActivity;
    }

    // Properties
    public static final String EXTRA_CATEGORY = "com.example.fooddeliveryapp.CATEGORY";
    public static final String EXTRA_RESTAURANT = "com.example.fooddeliveryapp.RESTAURANT";
    public static final String EXTRA_ITEM = "com.example.fooddeliveryapp.ITEM";
    public HashMap<String,Restaurant> restaurantHashmap = new HashMap<>();
    public ArrayList<Restaurant> restaurantsArrayList = new ArrayList<>();
    public ArrayList<Category> categoryArrayList = new ArrayList<>();
    private final ArrayList<Restaurant> recommendedArrayList = new ArrayList<>();
    private final ArrayList<Restaurant> discountsArrayList = new ArrayList<>();
    private final ArrayList<Restaurant> popularArrayList = new ArrayList<>();
    private final ArrayList<Restaurant> highestRatedArrayList = new ArrayList<>();
    private final ArrayList<Food> featuredDishes = new ArrayList<>();
    private final ArrayList<Restaurant> featuredDishesRestaurant = new ArrayList<>();
    private int featuredDishPosition = 0;
    private String user;
    // XML Views
    private ImageView featuredImage;
    private ImageButton featuredImageButton1;
    private ImageButton featuredImageButton2;
    private ImageButton featuredImageButton3;
    private ImageButton featuredImageButton4;
    private ImageButton featuredImageButton5;
    private ImageButton selectAddressButton;
    public TextView currentAddressText;
    private RecyclerView miniCategoryRecyclerView;
    private RecyclerView recommendedRecyclerView;
    private RecyclerView discountsRecyclerView;
    private RecyclerView popularRecyclerView;
    private RecyclerView highestRatedRecyclerView;
    private BottomNavigationView bottomNavigationView;
}