package com.example.fooddeliveryapp;

import java.util.ArrayList;

public class Category {
    public String name;
    public int imageResource;
    public ArrayList<Restaurant> restaurants = new ArrayList<>();

    Category(String name, int imageResource) {
        this.name = name;
        this.imageResource = imageResource;
    }
}
