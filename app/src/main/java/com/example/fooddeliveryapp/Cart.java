package com.example.fooddeliveryapp;

import java.util.ArrayList;

public class Cart {

    //Singleton
    public static Cart getInstance() {
        if (sharedInstance == null) {
            sharedInstance = new Cart();
        }
        return sharedInstance;
    }
    private static Cart sharedInstance = null;
    private Cart() {} // Don't allow instantiation outside of this class

    //Public properties
    public Integer numberOfItems() {
        Integer numberOfItems = 0;
        for (CartItem cartItem : cartItems) {
            numberOfItems += cartItem.getQuantity();
        }
        return numberOfItems;
    }

    public Integer totalPrice() {
        Integer totalPrice = 0;
        for (CartItem cartItem : cartItems) {
            totalPrice += cartItem.subtotalPrice();
        }
        return totalPrice;
    }

    public void add(Food food, Integer quantity) {
        //Check if food is already in cart
        CartItem existingCartItem = null;
        for (CartItem cartItem : cartItems) {
            if (cartItem.food.equals(food)) {
                existingCartItem = cartItem;
                break;
            }
        }
        if (existingCartItem != null) {
            // Found matching dish in cart
            existingCartItem.quantity += quantity;
        }
        else {
            // Found no matching dish in cart
            CartItem newCartItem = new CartItem(food, quantity);
            cartItems.add(newCartItem);
        }
    }

    //Private property
    private ArrayList<CartItem> cartItems = new ArrayList<>();

    // Nested classes
    public class CartItem {

        //Constructor
        public CartItem(Food food, Integer quantity) {
            this.food = food;
            this.quantity = quantity;
        }

        //Public methods
        public Integer getQuantity() {
            return quantity;
        }

        public Integer itemPriceInCents() {
            return food == null ? 0 : food.price;
        }

        public String foodName() {
            return food == null ? "" : food.name;
        }

        public Integer subtotalPrice() {
            return quantity * itemPriceInCents();
        }

        //Private properties
        private Food food;
        private Integer quantity;
    }
}
