package com.example.fooddeliveryapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class FoodItemActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_item);

        connectXMLViews();
        setupListeners();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Restaurant restaurant = DataModel.getInstance(this).retrieveRestaurants().get(getIntent().getExtras().getInt("com.example.fooddeliveryapp.RESTAURANT"));
        Food item = restaurant.allItemsHashMap.get(getIntent().getExtras().getString("com.example.fooddeliveryapp.ITEM"));
        assert item != null;
        displayImage.setImageResource(item.imageResource);
        itemName.setText(item.name);
        String price;
        if (item.price == 0) { price = "Free"; }
        else if (item.price/100 == 0) { price = "$0." + item.price % 100; }
        else { price = "$" + item.price/100 + "." + item.price % 100; }
        itemPrice.setText(price);
        itemDescription.setText(item.description);

        System.out.println("FoodItemActivity populated with " + item.name);
    }

    // Sets up listeners for user interaction
    private void setupListeners() {
        removeItem.setOnClickListener(v -> {
            System.out.println("The user clicked on the remove item button.");
            //check if amount is more than 0
            if (currentNumber > 0) {
                currentNumber -= 1;
            }
            //Display the current number
            String text = currentNumber + "";
            itemAmount.setText(text);
        });

        addItem.setOnClickListener(v -> {
            System.out.println("The user clicked on the add item button.");
            currentNumber += 1;
            //Display the current number
            String text = currentNumber + "";
            itemAmount.setText(text);
        });

        cartButton.setOnClickListener(v -> {
            System.out.println("Added to cart");
        });
    }

    //Connect XML views
    private void connectXMLViews() {
        displayImage = findViewById(R.id.display_image);
        itemName = findViewById(R.id.item_name);
        itemPrice = findViewById(R.id.item_price);
        itemDescription = findViewById(R.id.item_description);
        itemAmount = findViewById(R.id.item_amount);
        removeItem = findViewById(R.id.remove_item);
        addItem = findViewById(R.id.add_item);
        cartButton = findViewById(R.id.add_to_cart);
    }

    // Properties
    int currentNumber = 0;
    // XML Views
    private ImageView displayImage;
    private TextView itemName;
    private TextView itemPrice;
    private TextView itemDescription;
    private TextView itemAmount;
    private ImageButton removeItem;
    private ImageButton addItem;
    private ImageButton cartButton;
}