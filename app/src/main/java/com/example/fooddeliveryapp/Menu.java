package com.example.fooddeliveryapp;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class Menu {
    // Properties
    public String name;
    public ArrayList<Food> foodItems;

    Menu(String name, ArrayList<Food> foodItems) {
        this.name = name;
        this.foodItems = foodItems;
    }
}
