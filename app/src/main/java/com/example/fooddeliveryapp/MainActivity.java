package com.example.fooddeliveryapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initializes variables
        lightBlue = getColorStateList(R.color.light_blue);
        white = getColorStateList(R.color.white);
        userRegistry = getSharedPreferences("User Registry", MODE_PRIVATE);
        System.out.println("User Registry currently contains: \n" + userRegistry.getAll());

        connectXLMViews();
        setOnClickListeners();
    }

    // Connects the XML views of the XML file into the activity
    private void connectXLMViews() {
        selectLoginButton = findViewById(R.id.login_button);
        selectLoginButtonImage = findViewById(R.id.login_button_image);
        selectSignupButton = findViewById(R.id.signup_button);
        selectSignupButtonImage = findViewById(R.id.signup_button_image);
        enterLoginID = findViewById(R.id.enter_login_id);
        enterPassword = findViewById(R.id.enter_password);
        incorrectPasswordMessage = findViewById(R.id.incorrect_password_message);
        signupErrorMessage = findViewById(R.id.signup_error_message);
        enterConfirmPassword = findViewById(R.id.enter_confirm_password);
        forgotPasswordButton = findViewById(R.id.forgot_password_button);
        confirmLoginButton = findViewById(R.id.login_final_button);
        confirmSignupButton = findViewById(R.id.signup_final_button);
        confirmLoginButtonText = findViewById(R.id.login_final_button_text);
        confirmSignupButtonText = findViewById(R.id.signup_final_button_text);
        gmailLoginButton = findViewById(R.id.login_gmail_button);
        twitterLoginButton = findViewById(R.id.login_twitter_button);
        facebookLoginButton = findViewById(R.id.login_facebook_button);
    }

    // Sets the onClickListeners for all buttons
    private void setOnClickListeners() {
        selectLoginButton.setOnClickListener(v -> {
            System.out.println("Login selected");
            switchToLogin();
        });

        selectSignupButton.setOnClickListener(v -> {
            System.out.println("Signup selected");
            switchToSignup();
        });

        forgotPasswordButton.setOnClickListener(v -> {
            System.out.println("Forgot password button clicked");
            // If the registry contains the id entered, show the password
            if (userRegistry.contains(enterLoginID.getText().toString())) {
                userRegistry.getString(enterLoginID.getText().toString(), null);
            }
            else {
                //TODO implement forgot password XML
            }
        });

        confirmLoginButton.setOnClickListener(v -> {
            System.out.println("Confirm login button clicked");
            // If the registry contains the entered user and the password is correct, login
            if (userRegistry.contains(enterLoginID.getText().toString()) && userRegistry.getString(enterLoginID.getText().toString(), null).equals(enterPassword.getText().toString())){
                login(enterLoginID.getText().toString());
            }
            else {
                incorrectPasswordMessage.setVisibility(View.VISIBLE);
                enterPassword.setText("");
                System.out.println("Incorrect id or password to login");
            }
        });

        confirmSignupButton.setOnClickListener(v -> {
            System.out.println("Confirm signup button clicked");
            // If the registry already contains the entered username, ask the user to make a new one
            if (!userRegistry.contains(enterLoginID.getText().toString())) {
                // If the entered passwords match, add the new user and their password to the registry
                if (enterPassword.getText().toString().equals(enterConfirmPassword.getText().toString())) {
                    SharedPreferences.Editor editor = userRegistry.edit();
                    editor.putString(enterLoginID.getText().toString(), enterPassword.getText().toString());
                    editor.apply();
                    System.out.println("User " + enterLoginID.getText() + " added to registry");
                    switchToLogin();
                }
                else {
                    signupErrorMessage.setText(R.string.not_matching_passwords_message_string);
                    signupErrorMessage.setVisibility(View.VISIBLE);
                    enterConfirmPassword.setText("");
                    System.out.println("Entered passwords do not match");
                }
            }
            else {
                signupErrorMessage.setText(R.string.username_already_used_string);
                signupErrorMessage.setVisibility(View.VISIBLE);
                System.out.println("Entered username \"" + enterLoginID.getText().toString() + "\" already exists in registry");
            }
        });

        gmailLoginButton.setOnClickListener(v -> {
            System.out.println("Gmail login button clicked");
            login("Gmail User");
        });

        twitterLoginButton.setOnClickListener(v -> {
            System.out.println("Twitter login button clicked");
            login("Twitter User");
        });

        facebookLoginButton.setOnClickListener(v -> {
            System.out.println("Facebook login button clicked");
            login("Facebook User");
        });
    }

    // Disables all views except for the login objects
    private void switchToLogin() {
        selectLoginButton.setTextColor(white);
        selectSignupButton.setTextColor(lightBlue);
        selectLoginButtonImage.setVisibility(View.VISIBLE);
        selectSignupButtonImage.setVisibility(View.INVISIBLE);
        enterPassword.setInputType(EditorInfo.TYPE_TEXT_VARIATION_PASSWORD);
        enterLoginID.setText("");
        enterPassword.setText("");
        enterConfirmPassword.setText("");
        incorrectPasswordMessage.setVisibility(View.INVISIBLE);
        signupErrorMessage.setVisibility(View.INVISIBLE);
        enterConfirmPassword.setVisibility(View.INVISIBLE);
        forgotPasswordButton.setVisibility(View.VISIBLE);
        confirmLoginButton.setVisibility(View.VISIBLE);
        confirmLoginButtonText.setVisibility(View.VISIBLE);
        confirmSignupButton.setVisibility(View.INVISIBLE);
        confirmSignupButtonText.setVisibility(View.INVISIBLE);
    }

    // Disables all views except for the signup objects
    private void switchToSignup() {
        selectLoginButton.setTextColor(lightBlue);
        selectSignupButton.setTextColor(white);
        selectSignupButtonImage.setVisibility(View.VISIBLE);
        selectLoginButtonImage.setVisibility(View.INVISIBLE);
        enterPassword.setInputType(EditorInfo.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        enterLoginID.setText("");
        enterPassword.setText("");
        enterConfirmPassword.setText("");
        incorrectPasswordMessage.setVisibility(View.INVISIBLE);
        signupErrorMessage.setVisibility(View.INVISIBLE);
        enterConfirmPassword.setVisibility(View.VISIBLE);
        forgotPasswordButton.setVisibility(View.INVISIBLE);
        confirmLoginButton.setVisibility(View.INVISIBLE);
        confirmLoginButtonText.setVisibility(View.INVISIBLE);
        confirmSignupButton.setVisibility(View.VISIBLE);
        confirmSignupButtonText.setVisibility(View.VISIBLE);
    }

    // Logs in the user provided and sends the username to the next activity
    private void login(String user) {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra(Intent.EXTRA_USER, user);
        startActivity(intent);
        System.out.println("Logged in and switched to home screen");
    }

    //Properties
    private ColorStateList lightBlue;
    private ColorStateList white;
    private SharedPreferences userRegistry;

    // XML Views
    private Button selectLoginButton;
    private ImageView selectLoginButtonImage;
    private Button selectSignupButton;
    private ImageView selectSignupButtonImage;
    private EditText enterLoginID;
    private EditText enterPassword;
    private TextView incorrectPasswordMessage;
    private EditText enterConfirmPassword;
    private TextView signupErrorMessage;
    private Button forgotPasswordButton;
    private ImageButton confirmLoginButton;
    private TextView confirmLoginButtonText;
    private ImageButton confirmSignupButton;
    private TextView confirmSignupButtonText;
    private ImageButton gmailLoginButton;
    private ImageButton twitterLoginButton;
    private ImageButton facebookLoginButton;
}