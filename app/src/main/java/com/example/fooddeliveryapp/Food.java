package com.example.fooddeliveryapp;

import androidx.annotation.NonNull;

import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;

import java.util.Random;

public class Food {
    // Properties
    public String name;
    public String description;
    public int price;
    public int imageResource;
    public boolean mostPopular;

    public Food(int imageResource) {
        this.imageResource = imageResource;
    }

    // Populates the food item with random properties
    public void PopulateRandomProperties() {
        int maxPrice = 19999;
        int minPrice = 0;
        Lorem lorem = LoremIpsum.getInstance();

        this.price = (int)(Math.random() * (maxPrice - minPrice + 1) + minPrice);
        this.mostPopular = new Random().nextBoolean();
        this.name = lorem.getWords(1, 4);
        this.description = lorem.getParagraphs(1, 1);
    }
}
