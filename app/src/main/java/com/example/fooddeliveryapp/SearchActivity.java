package com.example.fooddeliveryapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        RecyclerView categoryRecyclerView = findViewById(R.id.category_recycler_view);

        // Sets up the category recyclerView from the DataModel
        ArrayList<Category> categoryArrayList = DataModel.getInstance(this).retrieveCategories();
        categoryRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        categoryRecyclerView.setAdapter(new CategoryAdapter(this, categoryArrayList, this));

        // Sets up the bottomNavigationView for user interaction
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            if (item.getItemId() == R.id.home_page) {
                Intent intent = new Intent(this, HomeActivity.class);
                this.startActivity(intent);
                return true;
            }
            else if (item.getItemId() == R.id.checkout_page) {
                Intent intent = new Intent(this, CheckoutActivity.class);
                this.startActivity(intent);
                return true;
            }
            return false;
        });

        System.out.println("SearchActivity created");
    }

    // Creates the options menu for the searchView
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.options_menu, menu);

        // Associates the searchView with the searchable configuration and the search menu
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    // Adapter for the categories inside of the categories recyclerView
    private static class CategoryViewHolder extends RecyclerView.ViewHolder {
        private CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryImage = itemView.findViewById(R.id.category_image);
            categoryName = itemView.findViewById(R.id.category_name);
        }

        // Properties
        private final ImageView categoryImage;
        private final TextView categoryName;
    }

    // Adapter for the categories inside of the categories recyclerView
    private static class CategoryAdapter extends RecyclerView.Adapter<CategoryViewHolder> {

        // Constructor
        private CategoryAdapter(@NonNull Context context, @NonNull ArrayList<Category> categories, @NonNull SearchActivity searchActivity) {
            this.context = context;
            this.categories = categories;
            this.searchActivity = searchActivity;
        }

        @NonNull
        @Override
        public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            // Inflates the viewHolder with the layout specified
            LayoutInflater inflater = LayoutInflater.from(context);
            View itemView = inflater.inflate(R.layout.item_search_category, parent, false);
            return new CategoryViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
            // Sets the values of the viewHolder to that of the current position in the list
            Category category = categories.get(position);
            holder.categoryImage.setImageResource(category.imageResource);
            holder.categoryName.setText(category.name);

            // Creates an onClickLister which activates when the holder is clicked
            holder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(searchActivity, CategoryActivity.class);
                intent.putExtra(EXTRA_CATEGORY, category.name);
                System.out.println("(Category) " + category.name + " selected");
                searchActivity.startActivity(intent);
            });
        }

        @Override
        public int getItemCount() {
            return categories.size();
        }

        // Properties
        private final Context context;
        private final ArrayList<Category> categories;
        private final SearchActivity searchActivity;
    }

    // Properties
    public static final String EXTRA_CATEGORY = "com.example.fooddeliveryapp.CATEGORY";
}